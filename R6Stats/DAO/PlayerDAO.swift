//
//  PlayerDAO.swift
//  R6Stats
//
//  Created by Corentin Stamper on 21/06/2017.
//  Copyright © 2017 R6Stats. All rights reserved.
//

import Foundation

class PlayerDAO {
    
    /// Insert player into database
    ///
    /// - Parameter player: The player that has to be stored in the database
    /// - Returns: return `true` if player has been added, `false` otherwise
    class func insert(_ player: Player) -> Bool {
        if PlayerDAO.exist(player) {
            return false
        }
        return player.commit()
    }
    
    /// List all stored players
    ///
    /// - Returns: `Array` contains all stored players
    class func listAll() -> [Player] {
        var players: [Player] = []
        for player in Player.query().fetch() {
            players.append(player as! Player)
        }
        return players
    }
    
    /// List all stored players for specific platform
    ///
    /// - Parameter platform: The platform of searched player
    /// - Returns: `Array` contains all stord players for selected platform
    class func listAll(forPlatform platform: String) -> [Player] {
        var players: [Player] = []
        for player in Player.query().where(withFormat: "platform = %@", withParameters: [platform]).fetch() {
            players.append(player as! Player)
        }
        return players
    }
    
    /// Delete player from database
    ///
    /// - Parameter player: The player that has to remove from the database
    /// - Returns: return `true` if player has been removed, `false` otherwise
    class func delete(_ player: Player) -> Bool {
        return player.remove()
    }
    
    /// Check if the player has already been into database
    ///
    /// - Parameter player: The player that has to be checked
    /// - Returns: return `true` if player exist, `false` otherwise
    private class func exist(_ player: Player) -> Bool {
        let playersSet = Player.query().where(withFormat: "username = %@ and platform = %@", withParameters: [(player.username)!, (player.platform)!]).fetch()
        return playersSet?.count != 0
    }
}
