//
//  PlayerViewCell.swift
//  R6Stats
//
//  Created by Corentin Stamper on 21/06/2017.
//  Copyright © 2017 R6Stats. All rights reserved.
//

import Foundation
import UIKit

class PlayerViewCell: UITableViewCell {
    
    @IBOutlet weak var username: UILabel!
}
