//
//  PCPlayersTableViewController.swift
//  R6Stats
//
//  Created by Corentin Stamper on 12/06/2017.
//  Copyright © 2017 R6Stats. All rights reserved.
//

import UIKit

class PCPlayersTableViewController: UITableViewController {
    
    // Close view controller
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Action to add PC player
    @IBAction func add(_ sender: Any) {
        let alert = UIAlertController(title: "Add PC player", message: "Enter username of PC player and press Add", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { textfield in
            textfield.tag = 1
            textfield.placeholder = "username"
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { _ in
            var username: String?
            for textfield in alert.textFields! {
                if textfield.tag == 1 {
                    username = textfield.text
                }
            }
            
            // Username must not be empty or have only spaces characters
            guard !(username?.replacingOccurrences(of: " ", with: "").isEmpty)! else {
                let error = UIAlertController(title: "Error", message: "Username must not be empty", preferredStyle: .alert)
                error.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.present(error, animated: true, completion: nil)
                return
            }
            
            let player = Player()
            player.username = username
            player.platform = "uplay"
            if PlayerDAO.insert(player) {
                self.tableView.reloadData()
            } else {
                let error = UIAlertController(title: "Error", message: "An error has occured while player saving", preferredStyle: .alert)
                error.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.present(error, animated: true, completion: nil)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PlayerDAO.listAll(forPlatform: "uplay").count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let players = PlayerDAO.listAll(forPlatform: "uplay")
        let cell = tableView.dequeueReusableCell(withIdentifier: "playerViewCell") as! PlayerViewCell
        cell.username.text = players[indexPath.row].username
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let players = PlayerDAO.listAll(forPlatform: "uplay")
        if editingStyle == .delete {
            let player = players[indexPath.row]
            if !PlayerDAO.delete(player) {
                let error = UIAlertController(title: "Error", message: "An error has occured while player deleting", preferredStyle: .alert)
                error.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                present(error, animated: true, completion: nil)
            }
        }
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let plr = PlayerDAO.listAll(forPlatform: "uplay")[indexPath.row]
        PlayerRequest.findStats(forPlayer: plr, completion: { player in
            
        })
    }
}
