//
//  Player.swift
//  R6Stats
//
//  Created by Corentin Stamper on 12/06/2017.
//  Copyright © 2017 R6Stats. All rights reserved.
//

import SharkORM

class Player: SRKObject {
    
    dynamic var username: String?
    dynamic var platform: String?
    
    var ubisoftId: String?
    var level: Int = 0
    var xp: Int = 0
    // Ranked Stats
    var rankedPlayed: Bool = false
    var rankedWins: Int = 0
    var rankedLoses: Int = 0
    var rankedwlr: Double = 0.0
    var rankedKills: Int = 0
    var rankedDeaths: Int = 0
    var rankedkdr: Double = 0.0
    var rankedPlaytime: Int = 0
    // Casual Stats
    var casualPlayed: Bool = false
    var casualWins: Int = 0
    var casualLoses: Int = 0
    var casualwlr: Double = 0.0
    var casualKills: Int = 0
    var casualDeaths: Int = 0
    var casualkdr: Double = 0.0
    var casualPlaytime: Int = 0
    // Overall Stats
    var revives: Int = 0
    var suicides: Int = 0
    var reinforcementsDeployed: Int = 0
    var barricadesBuilt: Int = 0
    var bulletsFired: Int = 0
    var bulletsHit: Int = 0
    var headshots: Int = 0
    var meleeKills: Int = 0
    var penetrationKills: Int = 0
    var assists: Int = 0
    
    override class func ignoredProperties() -> [Any] {
        return ["ubisoftId", "level", "xp",
                "rankedPlayed", "rankedWins", "rankedLoses", "rankedwlr", "rankedKills", "rankedDeaths", "rankedkdr", "rankedPlaytime",
                "casualPlayed", "casualWins", "casualLoses", "casualwlr", "casualKills", "casualDeaths", "casualkdr", "casualPlaytime",
                "revives", "suicides", "reinforcementsDeployed", "barricadesBuilt", "bulletsFired", "bulletsHit", "headshots", "meleeKills", "penetrationKills", "assists"]
    }
}
