//
//  PlayerRequest.swift
//  R6Stats
//
//  Created by Corentin Stamper on 21/06/2017.
//  Copyright © 2017 R6Stats. All rights reserved.
//

import Foundation
import Alamofire

class PlayerRequest {
    
    class func findStats(forPlayer player: Player, completion: @escaping (Player?) -> Void) {
        let url = "https://api.r6stats.com/api/v1/players/\(String(describing: player.username!))?platform=\(String(describing: player.platform!))"
        print(url)
        Alamofire.request(url, method: .get)
            .validate()
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
            })
    }
}
